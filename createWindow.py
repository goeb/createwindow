# Copyright 2009, 2014 Stefan Goebel - <createwindow -at- subtype -dot- de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
createWindow 0.2 - Create simple polygon window geometry.

This module provides the createWindow() function, please see the description of
this function for details.
"""


__version = "0.2"


import maya.cmds as cmd
import math
import polyFunctions as pf


def createWindow (
        name        = "window",
        wsHeight    = 0,
        wsInside    = 0,
        wsOutside   = 0,
        ofWidth     = 0,
        ofHeight    = 0,
        ofDepth     = 0,
        ofOffset    = 0,
        ifWidth     = 0,
        ifHeight    = 0,
        ifDepth     = 0,
        glass       = 0,
        splitV      = 0,            # number of inner frame |
        splitH      = 0,            # number of inner frame -
        hTolerance  = 0.1,
        opOffset    = 0.001,
        opTolerance = 0.000001,
        opDistance  = 100
    ):

    """Create simple polygon window geometry.

    A window with the given specifications will be created based on the
    selected faces of a polygon mesh. The faces and their opposite faces,
    based on information from the oppositeFace plugin, will be deleted, the
    edges bridged and the window will be created in the resulting hole.

    Parameters (default to 0 unless specified otherwise):

        name            base name of the window components, default "window"
        wsHeight        height of the windowsill
        wsInside        length of windowsill from the inside wall
        wsOutside       length of windowsill from the outside wall
        ofWidth         outer frame width
        ofHeight        outer frame height
        ofDepth         outer frame depth
        ofOffset        offset of the outer frame from the outside wall
        ifWidth         inner frame width
        ifHeight        inner frame height
        ifDepth         inner frame depth
        glass           depth of the glass
        splitV          divide this many times in vertical direction
        splitH          divide this many times in horizontal direction
        hTolerance      tolerance for grouping vertices by height (default 0.1)
        opOffset        offset for oppositeFace plugin (default 0.001)
        opTolerance     tolerance for oppositeFace plugin (default 1e-6)
        opDistance      maximum distance for oppositeFace plugin (default 100)

    Creation of a windowSill will be disabled if wsHeight is set to 0
    (default). Everything else must be set to reasonable values (splitV and
    splitH may be left with their default value of 0 if no further divisions of
    the window are required).

    The function needs to find the top and bottom vertices of the created hole.
    If this does not work correctly, try to modify the hTolerance parameter.

    All components will be grouped and parented to the original mesh, the group
    name will be returned. If anything goes wrong, False will be returned. In
    that case you may be left with incomplete window geometry!

    This function requires the oppositeFace plugin and polyFunctions.py to be
    loaded before execution.
    """

    # Bridge the faces:
    (ok, front, back) = pf.bridgeOppositeFaces (
            offset = opOffset, tolerance = opTolerance, distance = opDistance
        )
    if not ok:
        return __error ("Faces could not be bridged.")

    # Find the bottom and the top vertices:
    fVByY  = __vertsByHeight (front ["vertices"], hTolerance)
    fVerts = []
    fVerts.append (fVByY [sorted (fVByY.keys ()) [ 0]])
    fVerts.append (fVByY [sorted (fVByY.keys ()) [-1]])

    # Now move the bottom and top vertices a bit so all end up with the same Y
    # coordinate (bottom and top separately, of course). The Y coordinate of
    # the top vertices will be saved in topY for later...
    topY = None
    for i in [0, 1]:
        temp = {}
        for v in fVerts [i]:
            temp [v] = front ["vertices"] [v] [1]
            temp [front ["oppositeVertices"] [v]] = \
                back ["vertices"] [front ["oppositeVertices"] [v]] [1]
        topY = __avgVertexHeight (temp)
        if topY is None:
            return __error ("Could not adjust vertex height.")

    # Bottom and top edges.
    fEdges = [[], []]
    for (e, (v1, v2)) in front ["edges"].items ():
        for i in [0, 1]:
            if v1 in fVerts [i] and v2 in fVerts [i]:
                fEdges [i].append (e)
        if len (fEdges [0]) == len (fVerts [0]) - 1 and \
                len (fEdges [1]) == len (fVerts [1]) - 1:
            break

    # Now iterate over the bottom and top edges and get their vertices, save
    # the number of times a specific vertex is found in the temporary
    # dictionaries. All vertices in the middle of the bottom front will be seen
    # twice (once for the left edge and once for the right one), the corner
    # vertices only once.
    temp = [{}, {}]
    for i in [0, 1]:
        for e in fEdges [i]:
            for v in front ["edges"] [e]:
                if temp [i].has_key (v):
                    temp [i] [v] = temp [i] [v] + 1
                else:
                    temp [i] [v] = 1

    # After that the fVerts array will contain only the top and bottom corner
    # vertices:
    for i in [0, 1]:
        for (v, j) in temp [i].items ():
            if j == 1:
                fVerts [i].append (v)
        fVerts [i] = fVerts [i] [-2:]

    # Now go along the edges of the front face, starting at the corner vertices
    # and moving upwards (that means moving along an edge that is not in the
    # bottom edge list), gather a list of vertices along the way until a top
    # corner vertex is reached. The vertices found will the be aligned to the
    # position of the bottom corner vertex.
    vertsAbove = {}
    for v in fVerts [0]:
        temp = [[v], [front ["oppositeVertices"] [v]], fEdges [0]]
        # While top corner not reached...
        while v not in fVerts [1]:
            # Iterate over all front face edges...
            for (e, (v1, v2)) in front ["edges"].items ():
                # Ignore it if the current vertex doesn't belong to the edge or
                # the edge is a bottom one or one already processed...
                if v not in (v1, v2) or e in temp [2]:
                    continue
                # Get the next vertex on the way to the top...
                if v == v2:
                    v = v1
                else:
                    v = v2
                temp [0].append (v)
                temp [1].append (front ["oppositeVertices"] [v])
                temp [2].append (e)
                break
        # Save which top corner vertex is located above a bottom one in
        # vertsAbove:
        for i in [0, 1]:
            vertsAbove [temp [i] [0]] = temp [i] [-1]
        # And finally adjust the position of the vertices:
        if not __vertsAdjustVertical (temp [0:2]):
            return __error ("Could not adjust vertex positions.")

    # cornerVerts will contain all eight corner vertices of the hole in the
    # wall, first four elements are the bottom ones, last four are the top
    # ones:
    cornerVerts = [
            front ["oppositeVertices"] [fVerts [0] [0]],
            fVerts [0] [0],
            fVerts [0] [1],
            front ["oppositeVertices"] [fVerts [0] [1]],
        ]
    # Reverse the array if required to get the right normal direction:
    if pf.polyArea2D (cornerVerts) > 0:
        cornerVerts.reverse ()
    for i in range (4):
        cornerVerts.append (vertsAbove [cornerVerts [i]])

    # All created elements will be saved in allElems...
    allElems = []

    # The windowsill:

    wsName = None
    if wsHeight > 0:
        # The vertices defining the bottom plane of the windowsill will be
        # calculated so that the front and back edges are parallel to the wall
        # (more precisely parallel to the line through the back and front
        # corner vertices):
        wsVerts = []
        for i in range (4):
            try:
                wsVerts.append (cmd.pointPosition (cornerVerts [i]))
            except (RuntimeError, TypeError), error:
                return __error (error)
        if wsOutside != 0:
            for (i, (x, y, z)) in zip ([1, 2], [[0, 1, 2], [3, 2, 1]]):
                wsVerts [i] = pf.pointInDist (
                        cornerVerts [x],
                        cornerVerts [y],
                        cornerVerts [z],
                        wsOutside
                    )
        if wsInside != 0:
            for (i, (x, y, z)) in zip ([0, 3], [[1, 0, 3], [2, 3, 0]]):
                wsVerts [i] = pf.pointInDist (
                        cornerVerts [x],
                        cornerVerts [y],
                        cornerVerts [z],
                        wsInside
                    )
        if None in wsVerts:
            return __error ("Error getting windowsill vertices.")
        wsName = "%s_ws_#" % name
        try:
            # Create the bottom plane of the windowsill and extrude it to the
            # specified height:
            wsName = cmd.polyCreateFacet (
                    p = pf.unitCorrection (wsVerts), n = wsName, ch = False
                ) [0]
            cmd.polyExtrudeFacet (ws = True, ty = wsHeight, ch = False)
        except (RuntimeError, TypeError), error:
            return __error (error)
        allElems.append (wsName)

    # cornerPoints will basically be the same as cornerVerts (see above), but
    # instead of vertex names it will contain the according coordinates, and
    # the height of the windowsill (if any) will be taken into account, too.
    cornerPoints = []
    for v in cornerVerts:
        if wsHeight > 0:
            cornerPoints.append (
                    map (
                            lambda x: x [0] + x [1],
                            zip (pf.pointCoordinates (v), (0, wsHeight, 0))
                        )
                )
        else:
            cornerPoints.append (pf.pointCoordinates (v))
    if None in cornerPoints:
        return __error ("Error getting corner vertex positions.")

    # The outer frame:

    #    0---1------2---3   ofBase will contain the coordinates of the outer
    #   /   /      /   /    frame corners. Please ignore all the for loops,
    #  7---6------5---4     they are just there to avoid doing basically the
    #                       same thing for all eight points separately.
    # At the end there will be 32 points in this array, aligned as shown here,
    # starting from the bottom.

    ofBase = 32 * [None]

    for (i, j, k, l) in [(7, 0, 1, 2), (4, 3, 2, 1)]:
        ofBase [i] = pf.pointInDist (
                cornerPoints [j],
                cornerPoints [k],
                cornerPoints [l],
                - ofOffset
            )

    for (i, j, k) in [(0, 7, 4), (3, 4, 7)]:
        ofBase [i] = pf.pointInDist (
                cornerPoints [i], ofBase [j], ofBase [k], - ofDepth
            )

    for (i, j, k, l, m) in [(7, 4, 0, 6, 1), (4, 7, 3, 5, 2)]:
        dir = pf.dirVector (ofBase [i], ofBase [j])
        dif = math.sqrt (
                # Floating point errors may lead to negative numbers for
                # perpendicular geometry (i think...), we need to use abs:
                abs (
                        pf.pointsDistance (ofBase [i], ofBase [k]) ** 2 -
                            ofDepth ** 2
                    )
            )
        if pf.innerProduct (dir, pf.dirVector (ofBase [i], ofBase [k])) < 0:
            ofBase [l] = pf.pointInDir (ofBase [i], dir, ofWidth)
            ofBase [m] = pf.pointInDir (ofBase [k], dir, ofWidth + dif)
        else:
            ofBase [l] = pf.pointInDir (ofBase [i], dir, ofWidth + dif)
            ofBase [m] = pf.pointInDir (ofBase [k], dir, ofWidth)

    # Now we calculate the remaining points, after that ofBase will contain
    # 24 more points (3 more layers), in the same order as specified above.

    for i in range (8):
        (x, y, z) = ofBase [i]
        ofBase [i +  8] = (x, ofBase [i] [1] + ofHeight, z)
        ofBase [i + 16] = (x, topY - ofHeight,           z)
        ofBase [i + 24] = (x, topY,                      z)

    # The outer frame will be built from several cuboids, again, the for loop
    # saves a few lines, nothing special going on in there...

    ofElems = []

    for (a, b, c, d) in [(0, 1, 6, 7), (1, 2, 5, 6), (2, 3, 4, 5)]:
        for e in [0, 8, 16]:
            if a == 1 and e == 8:
                continue
            (i, j, k, l) = map (lambda x: x + e, [a, b, c, d])
            (m, n, o, p) = map (lambda x: x + 8, [i, j, k, l])
            area = pf.polyArea2D (
                    [ofBase [i], ofBase [j], ofBase [k], ofBase [l]]
                )
            if area is None:
                return __error ("Error getting polygon area.")
            if area > 0:
                points = [
                        ofBase [i], ofBase [j], ofBase [k], ofBase [l],
                        ofBase [m], ofBase [n], ofBase [o], ofBase [p],
                    ]
            else:
                points = [
                        ofBase [l], ofBase [k], ofBase [j], ofBase [i],
                        ofBase [p], ofBase [o], ofBase [n], ofBase [m],
                    ]
            ofName = pf.cuboidFromPoints (*points)
            if ofName is None:
                return __error ("Could not build outer frame.")
            ofElems.append (ofName)

    # Merge the single cuboids of the outer frame, merge the vertices and
    # finally remove duplicate faces:

    ofName = "%s_of_#" % name
    try:
        ofName = cmd.polyUnite (ofElems, ch = False, n = ofName) [0]
        cmd.polyMergeVertex (
                "%s.vtx[*]" % ofName, d = 0.000001, am = True, ch = False
	        )
    except (RuntimeError, TypeError), error:
        return __error (error)
    if pf.removeDuplicateFaces (ofName) != 16:
        return __error ("Error removing outer frame faces.")
    allElems.append (ofName)

    # The inner frame and glass:

    ifBase = []
    for i in [9, 10, 13, 14, 17, 18, 21, 22]:
        ifBase.append (ofBase [i])

    height  = pf.pointsDistance (ifBase [0], ifBase [4])
    width   = pf.pointsDistance (ifBase [0], ifBase [1])
    gHeight = (height - ifHeight * splitH) / (splitH + 1)
    gWidth  = (width  - ifWidth  * splitV) / (splitV + 1)
    dirZ    = pf.dirVector (ifBase [3], ifBase [0])
    dirX    = pf.dirVector (ifBase [0], ifBase [1])
    center  = pf.pointBetween (ifBase [0], ifBase [3])
    ifElems = {"glass": [], "frame": []}

    for i in range (splitH + 1):

        gY = pf.pointInDir (center, (0, 1, 0), i * gHeight + i * ifHeight)
        fY = pf.pointInDir (
                center, (0, 1, 0), i * gHeight + (i - 1) * ifHeight
            )

        for j in range (splitV + 1):

            gX = pf.pointInDir (gY, dirX, j * gWidth + j * ifWidth)

            # A glass element:
            ifElems ["glass"].append (
                    __makeIFPart (
                            gX,
                            glass, dirZ, gWidth, dirX, gHeight,
                            "%s_g_%s_%s_#" % (name, i, j)
                        )
                )

            # Inner frame vertical element:
            if i > 0:
                ifElems ["frame"].append (
                        __makeIFPart (
                                pf.pointInDir (gX, (0, -1, 0), ifHeight),
                                ifDepth, dirZ, gWidth, dirX, ifHeight,
                                "%s_ifh_%s_%s_#" % (name, i, j)
                            )
                    )
            # Inner frame horizontal element:
            if j > 0:
                ifElems ["frame"].append (
                        __makeIFPart (
                                pf.pointInDir (gX, dirX, ifWidth, -1),
                                ifDepth, dirZ, ifWidth, dirX, gHeight,
                                "%s_ifv_%s_%s_#" % (name, i, j)
                            )
                    )
            # That little inner frame element where vertical and horizontal
            # elements intersect:
            if i > 0 and j > 0:
                ifElems ["frame"].append (
                        __makeIFPart (
                                pf.pointInDir (
                                        pf.pointInDir (gX, dirX, ifWidth, -1),
                                        (0, -1, 0),
                                        ifHeight
                                    ),
                                ifDepth, dirZ, ifWidth, dirX, ifHeight,
                                "%s_ifx_%s_%s_#" % (name, i, j)
                            )
                    )

    if None in ifElems ["glass"] or None in ifElems ["frame"]:
        return __error ("Could not create one or more inner elements.")

    # Now all that's left to do is combine some meshes and group some of the
    # stuff together...

    if len (ifElems ["glass"]) > 1:
        try:
            grp = cmd.group (ifElems ["glass"], w = True, n = "%s_g_#" % name)
        except (RuntimeError, TypeError), error:
            return __error (error)
        allElems.append (grp)
    elif len (ifElems ["glass"]) == 1:
        allElems.append (ifElems ["glass"] [0])
    else:
        return __error ("No glass? This should not have happened...")

    if len (ifElems ["frame"]) > 1:
        fName = "%s_if_#" % name
        try:
            fName = cmd.polyUnite (
                    ifElems ["frame"], ch = False, n = fName
                ) [0]
            cmd.polyMergeVertex (
                    "%s.vtx[*]" % fName, d = 0.000001, am = True, ch = False
                )
        except (RuntimeError, TypeError), error:
            return __error (error)
        if pf.removeDuplicateFaces (fName) != splitV * splitH * 8:
            return __error ("Error removing inner frame faces.")
        allElems.append (fName)
    elif len (ifElems ["frame"]) == 1:
        allElems.append (ifElems ["frame"] [0])

    # Window group will be parented to the original mesh:
    mesh = pf.parentTransform (front ["mesh"])
    try:
        grp = cmd.group (allElems, p = mesh, n = "%s_#" % name)
    except (RuntimeError, TypeError), error:
        return __error (error)

    return grp


def __avgVertexHeight (verts):

    """Move vertices to their average Y coordinate.

    Internal use only. The parameter must be a dictionary, the keys must be the
    vertex names, the values their current Y coordinate. All vertices will be
    moved in to their average Y coordinate, while keeping their X and Z
    coordinates. Returns the average Y value, or None in case of an error.
    """

    a = sum (verts.values ()) / float (len (verts))

    for (v, y) in verts.items ():
        if a != y:
            try:
                cmd.polyMoveVertex (v, ws = True, ty = a - y)
            except (RuntimeError, TypeError), error:
                return __error (error, None)

    return a


def __error (msg = "Unknown error.", ret = False):

    """Print an error message and return False.

    Internal use only. The parameter msg specifies the error message, it
    defaults to "Unknown error". Return value can be set with the ret
    parameter, it defaults to False.
    """

    print "Error: %s" % msg
    return ret


def __getPlane (p, depth, dirD, width, dirW):

    """No short summary available...

    Internal use only. Takes five arguments, in the following order: a point
    (specified either as vertex name or as an (x, y, z)-tuple), a depth and the
    direction vector for the depth, and a width and the direction vector for
    the width. Returns a list with four coordinates of the vertices that define
    the resulting plane, or None in case of an error.
    """

    plane = 4 * [None]

    plane [0] = pf.pointInDir (p, dirD, depth / 2.0,  1)
    plane [3] = pf.pointInDir (p, dirD, depth / 2.0, -1)
    plane [1] = pf.pointInDir (plane [0], dirW, width)
    plane [2] = pf.pointInDir (plane [3], dirW, width)

    if None in plane:
        return __error ("Could not get base plane.", None)

    if pf.polyArea2D (plane) > 0:
        plane.reverse ()

    return plane


def __makeIFPart (p, z, dirZ, x, dirX, height, name):

    """Create a single part of the inner frame or glass.

    Internal use only. First five parameters are used to create the bottom
    polygon of the part and are directly passed to __getPlane, see there for
    details. Two more parameters define the height of the part and the base
    name for the part. The function returns the name of the created component,
    or None in case of an error.
    """

    plane = __getPlane (p, z, dirZ, x, dirX)

    if plane is None:
        return __error ("Error getting base plane.", None)

    try:
        name = cmd.polyCreateFacet (
                p = pf.unitCorrection (plane), n = name, ch = False
            ) [0]
        cmd.polyExtrudeFacet (ws = True, ty = height, ch = False)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    return name


def __vertsAdjustVertical (verts):

    """Wrapper around vertsAdjustVertical.

    Internal use only. Works like vertsAdjustVertical, but the parameter must
    be a two-dimensional list, each sub-list is processed by
    vertsAdjustVertical separately. Returns True if everything went fine, False
    in case of an error.
    """

    return False not in map (pf.vertsAdjustVertical, verts)


def __vertsByHeight (verts, tolerance = 0.1):

    """Group vertices by their Y coordinate.

    Internal use only. The first parameter must be a dictionary, the keys are
    vertex names, the values are their coordinates as (x, y, z)-tuple. The
    function will sort these vertices by their height, they will be grouped in
    a dictionary with keys being the Y coordinates, and values being lists of
    the vertices with the according Y coordinate. A tolerance named parameter
    (defaults to 0.1) will include all vertices within the tolerance of an
    already existing key in this key's list (that means that the actual Y
    values of the vertices may differ from the key). The dictionary will be
    returned eventually.
    """

    ys = {}

    for (v, p) in verts.items ():
        for y in ys.keys ():
            if abs (y - p [1]) <= tolerance:
                ys [y].append (v)
                break
        else:
            ys [p [1]] = [v]

    return ys


print "createWindow.py version %s imported" % __version


# vim: set et si nofoldenable ft=python sts=4 sw=4 tw=79 ts=4 fenc=utf8 :
# :indentSize=4:tabSize=4:noTabs=true:mode=python:maxLineLen=79: